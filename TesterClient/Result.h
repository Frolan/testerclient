#pragma once

#include <QWidget>
#include "ui_Result.h"

class Result : public QWidget
{
	Q_OBJECT

public:
	Result(QWidget *parent = Q_NULLPTR);
	~Result();

	void setData(QString student, QString group, QString variant, int time, float currScore, float maxScore);

private:
	Ui::Result ui;

};
