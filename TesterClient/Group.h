#pragma once
#include <qstring.h>
#include "Student.h"
#include <qlist.h>
#include "stdafx.h"

class Group
{
public:
	Group();
	Group(int id, QString name, QList <Student> students);
	~Group();

	int id;
	QString name;
	QList <Student> students;
};

QDataStream & operator<< (QDataStream & out, const Group & group);
QDataStream & operator>> (QDataStream & in, Group & group);
