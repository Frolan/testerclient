#pragma once
#include <qstring.h>
#include <qlist.h>
#include "Question.h"

class Variant
{
public:
	Variant();
	Variant(int id, QString name);
	~Variant();

	int id;
	QString name;
	QList <Question> questions;
};

QDataStream & operator<< (QDataStream & out, const Variant & variant);
QDataStream & operator>> (QDataStream & in, Variant & variant);
