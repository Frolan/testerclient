#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_TesterClient.h"
#include <qtcpsocket.h>
#include <QMessageBox>
#include "Student.h"
#include "Group.h"
#include "stdafx.h"
#include "Variant.h"
#include "Testing.h"
#include "QuestionData.h"

class Testing;
class TesterClient : public QMainWindow
{
	Q_OBJECT

public:
	TesterClient(QWidget * pwgt = 0);

private:
	Ui::TesterClientClass ui;

	Testing * testingWindow;

	QTcpSocket * tcpSocket;
	quint16 nextBlockSize;

	QString host;
	int port;

	bool isConnected = false;

	QList <Group> groups;
	QList <Variant> variants;
	QList <QuestionData> questions;
	QString dataType = "";

	void connectToServer();
	void fillGroups();
	void fillVariants();
	void saveImages(QList <QImage> & images);
	void createDir(QString dirName);
	void saveFile(quint64 fileSize, QString fileName, QDataStream & in);

private slots:
	void slotReadyRead();
	void slotError(QAbstractSocket::SocketError);
	void slotSendToServer(QString str);
	void slotConnected();
	void slotFillStudents(int groupIndex);
	void slotStartTest();
};
