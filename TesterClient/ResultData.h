#pragma once
#include <qstring.h>
#include <QDataStream>

class ResultData
{
public:
	ResultData();
	ResultData(int id, int idStudent, int idVariant, float score, float maxScore, int time, QString date, int countTrueAnswer, int maxCountAnswer, QString answers);
	~ResultData();

	int id;
	int idStudent;
	int idVariant;
	float score;
	float maxScore;
	int time;
	QString date;
	int countTrueAnswer;
	int maxCountAnswer;
	QString answers;
};

QDataStream & operator<< (QDataStream & out, const ResultData & question);
QDataStream & operator>> (QDataStream & in, ResultData & question);