#include "Group.h"

Group::Group()
{
}

Group::Group(int id, QString name, QList <Student> students)
{
	this->id = id;
	this->name = name;
	this->students = students;
}

Group::~Group()
{
}

QDataStream & operator<< (QDataStream & out, const Group & group)
{
	out << group.id << group.name << group.students;
	return out;
}

QDataStream & operator>> (QDataStream & in, Group & group)
{
	in >> group.id >> group.name >> group.students;
	return in;
}
