#include "ResultData.h"

ResultData::ResultData()
{
}

ResultData::ResultData(int id, int idStudent, int idVariant, float score, float maxScore, int time, QString date, int countTrueAnswer, int maxCountAnswer, QString answers)
{
	this->idStudent = idStudent;
	this->idVariant = idVariant;
	this->score = score;
	this->maxScore = maxScore;
	this->time = time;
	this->date = date;
	this->countTrueAnswer = countTrueAnswer;
	this->answers = answers;
	this->maxCountAnswer = maxCountAnswer;
}

ResultData::~ResultData()
{
}

QDataStream & operator<< (QDataStream & out, const ResultData & question)
{
	out << question.answers << question.countTrueAnswer << question.maxCountAnswer << question.date << question.id << question.idStudent <<
		question.idVariant << question.maxScore << question.score << question.time;
	return out;
}

QDataStream & operator>> (QDataStream & in, ResultData & question)
{
	in >> question.answers >> question.countTrueAnswer >> question.maxCountAnswer >> question.date >> question.id >> question.idStudent >>
		question.idVariant >> question.maxScore >> question.score >> question.time;
	return in;
}
