#pragma once
#include <qstring.h>
#include <qglobal.h>
#include <qdatastream.h>
class Question
{
public:
	Question();
	Question(int id, QString name, QString questionFilename, QString referenceFilename,
		QString answerFilename, int answerType, int maxScore, int numberOfAttempts);
	~Question();

	int id;
	QString name;
	QString questionFilename;
	QString referenceFilename;
	QString answerFilename;
	int answerType;
	int maxScore;
	int numberOfAttempts;
};

QDataStream & operator<< (QDataStream & out, const Question & question);
QDataStream & operator>> (QDataStream & in, Question & question);