#include "QuestionData.h"

QuestionData::QuestionData()
{
}

QuestionData::QuestionData(QString question, int answerType, QStringList answers, int correct, int numberOfAttempts, int maxScore, QString reference)
{
	this->question = question;
	this->answerType = answerType;
	this->answers = answers;
	this->correct = correct;
	this->numberOfAttempts = numberOfAttempts;
	this->maxScore = maxScore;
	this->reference = reference;
}

QuestionData::~QuestionData()
{
}

QDataStream & operator<< (QDataStream & out, const QuestionData & question)
{
	out << question.question << question.answerType << question.answers << question.correct << question.numberOfAttempts
		<< question.maxScore << question.reference;
	return out;
}

QDataStream & operator>> (QDataStream & in, QuestionData & question)
{
	in >> question.question >> question.answerType >> question.answers >> question.correct >> question.numberOfAttempts
		>> question.maxScore >> question.reference;
	return in;
}