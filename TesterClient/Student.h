#pragma once
#include <qstring.h>
#include <qdatastream.h>
class Student
{
public:
	Student();
	Student(int id, QString name);
	~Student();

	int id;
	QString name;
};

QDataStream & operator<< (QDataStream & out, const Student & student);
QDataStream & operator>> (QDataStream & in, Student & student);

