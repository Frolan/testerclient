#pragma once
#include <qstring.h>
#include <qstringlist.h>
#include <qdatastream.h>

class QuestionData
{
public:
	QuestionData();
	QuestionData(QString question, int answerType, QStringList answers, int correct, int numberOfAttempts, int maxScore, QString reference);
	~QuestionData();

	QString question;
	int answerType;
	QStringList answers;
	int correct;
	int numberOfAttempts;
	int maxScore;
	QString reference;
};

QDataStream & operator<< (QDataStream & out, const QuestionData & question);
QDataStream & operator>> (QDataStream & in, QuestionData & question);