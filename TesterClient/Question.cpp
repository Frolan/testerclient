#include "Question.h"

Question::Question()
{

}

Question::Question(int id, QString name, QString questionFilename, QString referenceFilename,
	QString answerFilename, int answerType, int maxScore, int numberOfAttempts)
{
	this->id = id;
	this->name = name;
	this->questionFilename = questionFilename;
	this->referenceFilename = referenceFilename;
	this->answerFilename = answerFilename;
	this->answerType = answerType;
	this->maxScore = maxScore;
	this->numberOfAttempts = numberOfAttempts;
}

Question::~Question()
{
}

QDataStream & operator<< (QDataStream & out, const Question & question)
{
	out << question.questionFilename << question.referenceFilename << question.answerFilename << question.answerType
		<< question.answerType << question.maxScore << question.numberOfAttempts;
	return out;
}

QDataStream & operator>> (QDataStream & in, Question & question)
{
	in >> question.questionFilename >> question.referenceFilename >> question.answerFilename >> question.answerType
		>> question.answerType >> question.maxScore >> question.numberOfAttempts;
	return in;
}