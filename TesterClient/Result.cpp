#include "Result.h"

Result::Result(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

Result::~Result()
{
}

void Result::setData(QString student, QString group, QString variant, int time, float currScore, float maxScore)
{
	ui.student->setText(student);
	ui.group->setText(group);
	ui.variant->setText(variant);
	ui.time->setText(QString::number(time++) + QString::fromLocal8Bit(" ���"));
	ui.score->setText(QString::number(currScore) + QString::fromLocal8Bit(" �� ") + QString::number(maxScore) + QString::fromLocal8Bit(" (") + QString::number(currScore/maxScore*100) + QString::fromLocal8Bit("%)"));
}
