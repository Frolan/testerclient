#include "stdafx.h"
#include "TesterClient.h"
#include <QtWidgets/QApplication>
#include <QTextCodec>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("Windows-1251"));
	TesterClient w;
	w.show();
	return a.exec();
}
