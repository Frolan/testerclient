/********************************************************************************
** Form generated from reading UI file 'Testing.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TESTING_H
#define UI_TESTING_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Testing
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_3;
    QLabel *studentInfo;
    QSpacerItem *horizontalSpacer_4;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *reference;
    QSpacerItem *horizontalSpacer;
    QTextEdit *text;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QLineEdit *freeAnswer;
    QVBoxLayout *listAnswer;
    QHBoxLayout *horizontalLayout_7;
    QLineEdit *answer1;
    QRadioButton *correct1;
    QHBoxLayout *horizontalLayout_6;
    QLineEdit *answer2;
    QRadioButton *correct2;
    QHBoxLayout *horizontalLayout_5;
    QLineEdit *answer3;
    QRadioButton *correct3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *attempts;
    QLabel *numberOfAttempts;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *send;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_6;
    QLabel *variant;
    QLabel *label_4;
    QLabel *question;
    QLabel *label_5;
    QLabel *score;
    QLabel *label_9;
    QLabel *time;
    QSpacerItem *horizontalSpacer_7;

    void setupUi(QWidget *Testing)
    {
        if (Testing->objectName().isEmpty())
            Testing->setObjectName(QStringLiteral("Testing"));
        Testing->resize(610, 566);
        verticalLayout_3 = new QVBoxLayout(Testing);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_3);

        studentInfo = new QLabel(Testing);
        studentInfo->setObjectName(QStringLiteral("studentInfo"));

        horizontalLayout_9->addWidget(studentInfo);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_4);


        verticalLayout_3->addLayout(horizontalLayout_9);

        groupBox = new QGroupBox(Testing);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        reference = new QPushButton(groupBox);
        reference->setObjectName(QStringLiteral("reference"));

        horizontalLayout->addWidget(reference);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout);

        text = new QTextEdit(groupBox);
        text->setObjectName(QStringLiteral("text"));

        verticalLayout_2->addWidget(text);


        verticalLayout_3->addWidget(groupBox);

        groupBox_2 = new QGroupBox(Testing);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        freeAnswer = new QLineEdit(groupBox_2);
        freeAnswer->setObjectName(QStringLiteral("freeAnswer"));

        verticalLayout->addWidget(freeAnswer);

        listAnswer = new QVBoxLayout();
        listAnswer->setSpacing(6);
        listAnswer->setObjectName(QStringLiteral("listAnswer"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        answer1 = new QLineEdit(groupBox_2);
        answer1->setObjectName(QStringLiteral("answer1"));
        answer1->setReadOnly(true);

        horizontalLayout_7->addWidget(answer1);

        correct1 = new QRadioButton(groupBox_2);
        correct1->setObjectName(QStringLiteral("correct1"));

        horizontalLayout_7->addWidget(correct1);


        listAnswer->addLayout(horizontalLayout_7);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        answer2 = new QLineEdit(groupBox_2);
        answer2->setObjectName(QStringLiteral("answer2"));
        answer2->setReadOnly(true);

        horizontalLayout_6->addWidget(answer2);

        correct2 = new QRadioButton(groupBox_2);
        correct2->setObjectName(QStringLiteral("correct2"));

        horizontalLayout_6->addWidget(correct2);


        listAnswer->addLayout(horizontalLayout_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        answer3 = new QLineEdit(groupBox_2);
        answer3->setObjectName(QStringLiteral("answer3"));
        answer3->setReadOnly(true);

        horizontalLayout_5->addWidget(answer3);

        correct3 = new QRadioButton(groupBox_2);
        correct3->setObjectName(QStringLiteral("correct3"));

        horizontalLayout_5->addWidget(correct3);


        listAnswer->addLayout(horizontalLayout_5);


        verticalLayout->addLayout(listAnswer);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        attempts = new QLabel(groupBox_2);
        attempts->setObjectName(QStringLiteral("attempts"));

        horizontalLayout_3->addWidget(attempts);

        numberOfAttempts = new QLabel(groupBox_2);
        numberOfAttempts->setObjectName(QStringLiteral("numberOfAttempts"));

        horizontalLayout_3->addWidget(numberOfAttempts);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        send = new QPushButton(groupBox_2);
        send->setObjectName(QStringLiteral("send"));

        horizontalLayout_4->addWidget(send);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);


        verticalLayout->addLayout(horizontalLayout_4);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        verticalLayout_3->addWidget(groupBox_2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_6);

        variant = new QLabel(Testing);
        variant->setObjectName(QStringLiteral("variant"));

        horizontalLayout_8->addWidget(variant);

        label_4 = new QLabel(Testing);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_8->addWidget(label_4);

        question = new QLabel(Testing);
        question->setObjectName(QStringLiteral("question"));

        horizontalLayout_8->addWidget(question);

        label_5 = new QLabel(Testing);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_8->addWidget(label_5);

        score = new QLabel(Testing);
        score->setObjectName(QStringLiteral("score"));

        horizontalLayout_8->addWidget(score);

        label_9 = new QLabel(Testing);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_8->addWidget(label_9);

        time = new QLabel(Testing);
        time->setObjectName(QStringLiteral("time"));

        horizontalLayout_8->addWidget(time);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_7);


        verticalLayout_3->addLayout(horizontalLayout_8);


        retranslateUi(Testing);

        QMetaObject::connectSlotsByName(Testing);
    } // setupUi

    void retranslateUi(QWidget *Testing)
    {
        Testing->setWindowTitle(QApplication::translate("Testing", "Testing", nullptr));
        studentInfo->setText(QString());
        groupBox->setTitle(QApplication::translate("Testing", "\320\227\320\260\320\264\320\260\320\275\320\270\320\265", nullptr));
        reference->setText(QApplication::translate("Testing", "\320\241\320\277\321\200\320\260\320\262\320\272\320\260", nullptr));
        groupBox_2->setTitle(QApplication::translate("Testing", "\320\236\321\202\320\262\320\265\321\202", nullptr));
        correct1->setText(QString());
        correct2->setText(QString());
        correct3->setText(QString());
        attempts->setText(QApplication::translate("Testing", "\320\236\321\201\321\202\320\260\320\273\320\276\321\201\321\214 \320\277\320\276\320\277\321\213\321\202\320\276\320\272:", nullptr));
        numberOfAttempts->setText(QApplication::translate("Testing", "0", nullptr));
        send->setText(QApplication::translate("Testing", "\320\236\321\202\320\277\321\200\320\260\320\262\320\270\321\202\321\214", nullptr));
        variant->setText(QString());
        label_4->setText(QApplication::translate("Testing", " \320\262\320\276\320\277\321\200\320\276\321\201:", nullptr));
        question->setText(QString());
        label_5->setText(QApplication::translate("Testing", "\321\202\320\265\320\272\321\203\321\211\320\260\321\217 \320\276\321\206\320\265\320\275\320\272\320\260:", nullptr));
        score->setText(QString());
        label_9->setText(QApplication::translate("Testing", "\320\276\321\201\321\202\320\260\320\262\321\210\320\265\320\265\321\201\321\217 \320\262\321\200\320\265\320\274\321\217:", nullptr));
        time->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Testing: public Ui_Testing {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TESTING_H
