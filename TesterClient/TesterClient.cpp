#include "TesterClient.h"

TesterClient::TesterClient(QWidget * pwgt) 
	: QMainWindow(pwgt), nextBlockSize(0)
{
	ui.setupUi(this);

	tcpSocket = new QTcpSocket(this);

	connectToServer();

	connect(tcpSocket, SIGNAL(connected()), SLOT(slotConnected()));
	connect(tcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
	connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),	this, SLOT(slotError(QAbstractSocket::SocketError)));

	connect(ui.groups, SIGNAL(currentIndexChanged(int)), this, SLOT(slotFillStudents(int)));

	connect(ui.start, SIGNAL(clicked()), this, SLOT(slotStartTest()));
}

void TesterClient::connectToServer()
{
	QString fileName = "config.txt";
	QFile file(fileName);
	if (file.open(QIODevice::ReadOnly))
	{
		QString text = file.readAll();
		bool isOk;
		QStringList list = text.split("\r\n", QString::SplitBehavior::SkipEmptyParts);
		if (list.length() == 2) {
			host = list[0];
			port = list[1].toInt(&isOk);
			if (!isOk) {
				QMessageBox::critical(this, QString::fromLocal8Bit("������ �������"), QString::fromLocal8Bit("���� � ip-����� ������� � ����� config.txt ������� �����������. ���������� ���������� ����������."));
				file.close();
				return;
			}
			tcpSocket->connectToHost(host, port);
		}
		else {
			QMessageBox::critical(this, QString::fromLocal8Bit("������ �������"), QString::fromLocal8Bit("���� � ip-����� ������� � ����� config.txt ������� �����������. ���������� ���������� ����������."));
			file.close();
			return;
		}
		file.close();
	}
	else
	{
		QMessageBox::critical(this, QString::fromLocal8Bit("������ �������"), QString::fromLocal8Bit("���������� ��������� ������, �.�. �� ������� ������� ���� config.txt. ���������, ��� �� ����������, � ������������� ����������."));
	}

}

void TesterClient::slotReadyRead()
{
	QDataStream in(tcpSocket);
	in.setVersion(QDataStream::Qt_5_10);

	for (;;) {
		if (!nextBlockSize) {
			if (tcpSocket->bytesAvailable() < sizeof(quint16)) {
				dataType = "";
				break;
			}

			// ���������, ����� ������ ���� �� ����
			if (dataType == "")
			{
				in >> nextBlockSize;
				in >> dataType;
			}

			if (dataType != "file")
				in >> nextBlockSize;
			else
				nextBlockSize = 0;
		}

		if (tcpSocket->bytesAvailable() < nextBlockSize) {
			break;
		}

		if (dataType == "groups")
		{
			in >> groups;
			// ��������� ������� ����������� � ������� � ���������
			fillGroups();
			dataType = "";
		}
		else if (dataType == "variants")
		{
			in >> variants;
			fillVariants();
			dataType = "";
		}
		else if(dataType == "questions")
		{
			in >> questions;
			testingWindow->setQuestions(questions);
			dataType = "";
		}
		else if (dataType == "images")
		{
			QList <QImage> images;
			in >> images;
			saveImages(images);
			dataType = "";
		}
		else if (dataType == "file")
		{
			quint64 fileSize;
			QString fileName;
			in >> fileSize;
			in >> fileName;
			saveFile(fileSize, fileName, in);
			dataType = "";
		}
			
		nextBlockSize = 0;
	}
}

void TesterClient::slotError(QAbstractSocket::SocketError err)
{
	if (err == QAbstractSocket::HostNotFoundError) {
		isConnected = false;
		QMessageBox::critical(this, QString::fromLocal8Bit("������ �������"), QString::fromLocal8Bit("���������� ������������ � ����� ") + QString::number(port) + QString::fromLocal8Bit(" ������� ") + host + QString::fromLocal8Bit(". ���������, ��� ������ ������� � ������ ������� � ����� �������� config.txt ������� ���������."));
	}
	else if (err == QAbstractSocket::RemoteHostClosedError) {
		isConnected = false;
		QMessageBox::critical(this, QString::fromLocal8Bit("������ �������"), QString::fromLocal8Bit("���������� � �������� ���������. ���������, ��� ������ ������� � ������������� ����������."));
	}
	else if (err == QAbstractSocket::ConnectionRefusedError) {
		isConnected = false;
		QMessageBox::critical(this, QString::fromLocal8Bit("������ �������"), QString::fromLocal8Bit("���������� ������������ � ����� ") + QString::number(port) + QString::fromLocal8Bit(" ������� ") + host + QString::fromLocal8Bit(". ���������, ��� ������ ������� � ������ ������� � ����� �������� config.txt ������� ���������."));
	}
	else {
		isConnected = false;
		QMessageBox::critical(this, QString::fromLocal8Bit("��������"), QString(tcpSocket->errorString()));
	}
}

void TesterClient::slotSendToServer(QString str)
{
	QByteArray  arrBlock;
	QDataStream out(&arrBlock, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_5_10);
	out << quint16(0) << str;

	out.device()->seek(0);
	out << quint16(arrBlock.size() - sizeof(quint16));

	tcpSocket->write(arrBlock);
}

void TesterClient::slotConnected()
{
	isConnected = true;
}

// --------------- ������ � ��������� -----------------
void TesterClient::fillGroups()
{
	for (int i = 0; i < groups.length(); i++)
	{
		ui.groups->addItem(groups[i].name);
	}
}

void TesterClient::slotFillStudents(int groupIndex)
{
	QList <Student> students = groups[groupIndex].students;
	ui.students->clear();
	for (int i = 0; i <students.length(); i++)
	{
		ui.students->addItem(students[i].name);
	}
}

void TesterClient::fillVariants()
{
	for (int i = 0; i < variants.length(); i++)
	{
		ui.variants->addItem(variants[i].name);
	}
}

void TesterClient::slotStartTest()
{
	if (isConnected)
	{
		testingWindow = new Testing(tcpSocket);
		// ��������� ������ ��������
		int currStudent = groups[ui.groups->currentIndex()].students[ui.students->currentIndex()].id;
		int currVariant = variants[ui.variants->currentIndex()].id;
		QString student = ui.students->currentText();
		QString group = ui.groups->currentText();
		QString variant = ui.variants->currentText();

		slotSendToServer("start_test");

		int variantId = variants[ui.variants->currentIndex()].id;
		slotSendToServer(QString::number(variantId));

		testingWindow->startTest(student, group, variant, currStudent, currVariant);
		testingWindow->show();
		this->close();	
	}
	else
		QMessageBox::critical(this, QString::fromLocal8Bit("��������"), QString::fromLocal8Bit("����������� ���������� � ��������. ���������, ��� ������ ������� � ������������� ����������."));
}

// ---------- ������ � ������� ------------
void TesterClient::saveImages(QList <QImage> & images)
{
	createDir("Images");
	for (int i = 0; i < images.length(); i++)
	{
		images[i].save("Images/" + images[i].text("name"));
	}
}

void TesterClient::createDir(QString dirName)
{
	QDir dir = QDir(dirName);
	if (!dir.exists())
	{
		QDir().mkdir(dirName);
	}
}

void TesterClient::saveFile(quint64 fileSize, QString fileName, QDataStream & in)
{
	createDir("References");

	// ���� � ������ �����
	QFile save(fileName);
	save.open(QFile::WriteOnly);
	QDataStream write(&save);
	write.setVersion(QDataStream::Qt_5_10);

	long int lBytesDone = 0; // ���� ������ �� �������
	long int lSize = fileSize;
	long int lBytes;

	while (lBytesDone < lSize) 
	{ 
		//���� ������� ������, ��� �������
		lBytes = 0;
	
		while (lBytes == 0) lBytes = tcpSocket->waitForReadyRead(-1);

		if (-1 == lBytes) 
		{ 
			QMessageBox::critical(this, QString::fromLocal8Bit("��������"), QString::fromLocal8Bit("�������� ������ ��� �������� ������. ����������, ��������� ������."));
			return;
		}

		QByteArray tmp;
		if(tcpSocket->bytesAvailable() + lBytesDone > lSize)
			tmp = tcpSocket->read(lSize - lBytesDone);
		else
			tmp = tcpSocket->readAll();
		lBytes += write.writeRawData(tmp.data(), tmp.size());
		lBytesDone += tmp.size();
	}
	save.close();
}