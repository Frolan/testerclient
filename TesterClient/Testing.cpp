#include "Testing.h"

Testing::Testing(QTcpSocket * tcpSocket, QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	this->tcpSocket = tcpSocket;

	currQuestion = 0;
	currScore = 0;
	maxScore = 0;
	countTrueAnswers = 0;
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("Windows-1251"));

	connect(ui.send, SIGNAL(clicked()), this, SLOT(slotSendAnswer()));
}

Testing::~Testing()
{
}

void Testing::startTest(QString student, QString group, QString variant, int studentId, int variantId)
{
	this->student = student;
	this->group = group;
	this->variant = variant;
	maxTime = 60;
	currTime = 0;
	timer = new QTimer();
	timer->setInterval(60000);
	connect(timer, SIGNAL(timeout()), this, SLOT(update()));
	ui.time->setText(QString::number(maxTime) + QString::fromLocal8Bit(" ���"));

	ui.studentInfo->setText(student + "  " + group);
	ui.variant->setText(variant);
	this->studentId = studentId;
	this->variantId = variantId;
}

void Testing::update()
{
	if (++currTime < maxTime)
	{
		ui.time->setText(QString::number(maxTime - currTime) + QString::fromLocal8Bit(" ���"));
	}
	else
	{
		// ������������ ���������� � �������� ������������
		showResult();
	}
}

void Testing::setQuestions(QList <QuestionData> questions)
{
	this->questions = questions;
	for (int i = 0; i < questions.length(); i++) {
		maxScore += questions[i].maxScore;
	}
	showQuestion();
}

void Testing::showQuestion()
{
	QuestionData curr = questions[currQuestion];
	ui.text->setHtml(curr.question);

	if (curr.answerType == 1)
	{
		ui.freeAnswer->setVisible(false);
		ui.answer1->setVisible(true);
		ui.answer2->setVisible(true);
		ui.answer3->setVisible(true);
		ui.correct1->setVisible(true);
		ui.correct2->setVisible(true);
		ui.correct3->setVisible(true);
		ui.correct1->setChecked(true);
		
		ui.answer1->setText(curr.answers[0].toLocal8Bit());
		ui.answer2->setText(curr.answers[1].toLocal8Bit());
		ui.answer3->setText(curr.answers[2].toLocal8Bit());
		maxAttempt = 1;
	}
	else
	{
		ui.freeAnswer->setVisible(true);
		ui.answer1->setVisible(false);
		ui.answer2->setVisible(false);
		ui.answer3->setVisible(false);
		ui.correct1->setVisible(false);
		ui.correct2->setVisible(false);
		ui.correct3->setVisible(false);
		ui.correct1->setChecked(false);

		ui.freeAnswer->clear();;
		maxAttempt = curr.numberOfAttempts;
	}
	currAttempt = 0;
	
	ui.question->setText(QString::number(currQuestion+1) + QString::fromLocal8Bit(" �� ") + QString::number(questions.length()));
	ui.score->setText(QString::number(currScore) + QString::fromLocal8Bit(" �� ") + QString::number(maxScore));
	ui.numberOfAttempts->setText(QString::number(maxAttempt-currAttempt));
	timer->start();
}

void Testing::slotSendAnswer()
{
	QuestionData q = questions[currQuestion];
	bool isOk = false;
	QString answer;
	QString trueAnswer;
	float score = 0;
	if (q.answerType == 1) {
		switch (q.correct) {
			case 1:
				if (ui.correct1->isChecked()) {
					isOk = true;
					answer = ui.answer1->text();
				}
				trueAnswer = ui.answer1->text();
				break;
			case 2:
				if (ui.correct2->isChecked()) {
					isOk = true;
					answer = ui.answer2->text();
				}
				trueAnswer = ui.answer2->text();
				break;
			case 3:
				if (ui.correct3->isChecked()) {
					isOk = true;
					answer = ui.answer3->text();
				}
				trueAnswer = ui.answer3->text();
				break;
		}
		if (isOk) {
			score = q.maxScore;
			currScore += q.maxScore;
		}
	}
	else {
		answer = ui.freeAnswer->text();
		trueAnswer = q.answers[0];
		if (ui.freeAnswer->text() == q.answers[0]) {
			isOk = true;
		}
		else {
			if (currAttempt < maxAttempt-1) {
				currAttempt++;
				QMessageBox::warning(this, QString::fromLocal8Bit("��������"), QString::fromLocal8Bit("����� ��� �������. � ��� �������� ") + QString::number(maxAttempt - currAttempt) + QString::fromLocal8Bit(" �������."));
				ui.numberOfAttempts->setText(QString::number(maxAttempt - currAttempt));
				return;
			}
		}
		if (isOk) {
			currScore += q.maxScore / (float)(currAttempt+1);
			score = q.maxScore / (float)(currAttempt + 1);
		}
	}

	countTrueAnswers++;

	// ��������� ������ �� ������ � ��������	
	resultDoc.append(QString::fromLocal8Bit("������ �") + QString::number(currQuestion + 1));
	resultDoc.append("\r\n");
	resultDoc.append(QString::fromLocal8Bit("�����: ") + answer);
	resultDoc.append("\r\n");
	resultDoc.append(QString::fromLocal8Bit("���������� �����: ") + trueAnswer);
	resultDoc.append("\r\n");
	resultDoc.append(QString::fromLocal8Bit("�������: ") + QString::number(currAttempt++));
	resultDoc.append("\r\n");
	resultDoc.append(QString::fromLocal8Bit("������: ") + QString::number(score) + QString::fromLocal8Bit(" �� ") + QString::number(q.maxScore));

	resultDoc.append("\r\n\r\n");

	showNextQuestion();
}

void Testing::showNextQuestion()
{
	if (questions.length() > ++currQuestion) {
		showQuestion();
	}
	else {
		showResult();
	}
}

void Testing::showResult()
{
	resultWindow = new Result();
	resultWindow->setData(student, group, variant, currTime, currScore, maxScore);
	resultWindow->show();

	// ������������ ��������� � ��������� ������������
	QString date = QDate::currentDate().toString("dd.MM.yyyy");
	ResultData data(-1, studentId, variantId, currScore, maxScore, currTime, date, countTrueAnswers, questions.length(), resultDoc);
	slotSendToServer(data);

	this->close();
}

void Testing::slotSendToServer(QString str)
{
	QByteArray  arrBlock;
	QDataStream out(&arrBlock, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_5_10);
	out << quint16(0) << str;

	out.device()->seek(0);
	out << quint16(arrBlock.size() - sizeof(quint16));

	tcpSocket->write(arrBlock);
}

void Testing::slotSendToServer(ResultData & result)
{
	QByteArray  arrBlock;
	QDataStream out(&arrBlock, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_5_10);

	slotSendToServer("result");

	out << quint16(0) << result;

	out.device()->seek(0);
	out << quint16(arrBlock.size() - sizeof(quint16));

	tcpSocket->write(arrBlock);
}