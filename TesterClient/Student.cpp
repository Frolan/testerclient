#include "Student.h"



Student::Student()
{
}

Student::Student(int id, QString name)
{
	this->id = id;
	this->name = name;
}

Student::~Student()
{
}

QDataStream & operator<< (QDataStream & out, const Student & student)
{
	out << student.id << student.name;
	return out;
}

QDataStream & operator>> (QDataStream & in, Student & student)
{
	in >> student.id >> student.name;
	return in;
}
