#pragma once

#include <QWidget>
#include "ui_Testing.h"
#include "QuestionData.h"
#include <qtimer.h>
#include <QTextCodec>
#include <qmessagebox.h>
#include "Result.h"
#include <qfile.h>
#include "ResultData.h"
#include <qdatetime.h>
#include <qtcpsocket.h>

class Testing : public QWidget
{
	Q_OBJECT

public:
	Testing(QTcpSocket * tcpSocket, QWidget *parent = Q_NULLPTR);
	~Testing();

	void startTest(QString student, QString group, QString variant, int studentId, int variantId);
	void setQuestions(QList <QuestionData> questions);

private:
	Ui::Testing ui;

	Result * resultWindow;

	QTcpSocket * tcpSocket;

	QString resultDoc;

	QString student, group, variant;
	int currQuestion;
	int studentId;
	int variantId;
	QList <QuestionData> questions;
	float currScore;
	float maxScore;
	int currAttempt;
	int maxAttempt;
	int countTrueAnswers;

	QTimer * timer;
	int maxTime;
	int currTime;

	void showQuestion();
	void showNextQuestion();
	void showResult();
	void slotSendToServer(QString str);
	void slotSendToServer(ResultData & result);

private slots:
	void slotSendAnswer();
	void update();
};
