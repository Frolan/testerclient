#include "Variant.h"

Variant::Variant()
{

}

Variant::Variant(int id, QString name)
{
	this->id = id;
	this->name = name;
}


Variant::~Variant()
{
}

QDataStream & operator<< (QDataStream & out, const Variant & variant)
{
	out << variant.id << variant.name << variant.questions;
	return out;
}

QDataStream & operator>> (QDataStream & in, Variant & variant)
{
	in >> variant.id >> variant.name >> variant.questions;
	return in;
}